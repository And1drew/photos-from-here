// Photos from here
// Key:
// 82d8f563086553d2e072711297ffefae
// Secret:
// 532711043d713b5c
const locationButton = document.getElementById("geoLocationButton")
const locationSpan = document.getElementById("locationSpan")

const options = {
    enableHighAccuracy: true,
    timeout: 5000,
};

const fallbackLocation = {
    latitude: 44.1049,
    longitude: 70.0468
};

const displayLocation = function (coords){
    console.log(`Latitude : ${coords.latitude}`);
    console.log(`Longitude : ${coords.longitude}`);
    locationSpan.innerText = `Latitude : ${coords.latitude}` + `  Longitude : ${coords.longitude}`
}
function success(pos) {
    displayLocation(pos.coords);
}
function error(err){
    displayLocation(fallbackLocation);
}

locationButton.onclick = function (){
    navigator.geolocation.getCurrentPosition(success, error, options);
}

function constructImageURL (photoObj) {
    return "https://farm" + photoObj.farm +
            ".staticflickr.com/" + photoObj.server +
            "/" + photoObj.id + "_" + photoObj.secret + ".jpg";
}

// flickr.photos.geo.photosForLocation
// const imageUrl = constructImageURL(response.photos.photo[0]);
